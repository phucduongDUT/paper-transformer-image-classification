# 1. Thêm các thư viện cần thiết
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

(X_train, y_train), (X_test, y_test) = mnist.load_data()
X_val, y_val = X_train[50000:60000,:], y_train[50000:60000]
X_train, y_train = X_train[:50000,:], y_train[:50000]
print(X_train.shape)

X_train = X_train.reshape(X_train.shape[0], 28, 28, 1)
X_val = X_val.reshape(X_val.shape[0], 28, 28, 1)
X_test = X_test.reshape(X_test.shape[0], 28, 28, 1)


Y_train = np_utils.to_categorical(y_train, 10)
Y_val = np_utils.to_categorical(y_val, 10)
Y_test = np_utils.to_categorical(y_test, 10)
print('Dữ liệu y ban đầu ', y_train[0])
print('Dữ liệu y sau one-hot encoding ',Y_train[0])


# model = Sequential()
#
# # Thêm Convolutional layer với 32 kernel, kích thước kernel 3*3
# # dùng hàm sigmoid làm activation và chỉ rõ input_shape cho layer đầu tiên
# model.add(Conv2D(32, (3, 3), activation='sigmoid', input_shape=(28, 28, 1)))
#
# # Thêm Convolutional layer
# model.add(Conv2D(32, (3, 3), activation='sigmoid'))
#
# # Thêm Max pooling layer
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# # Flatten layer chuyển từ tensor sang vector
# model.add(Flatten())
#
# # Thêm Fully Connected layer với 128 nodes và dùng hàm sigmoid
# model.add(Dense(128, activation='sigmoid'))
#
# # Output layer với 10 node và dùng softmax function để chuyển sang xác xuất.
# model.add(Dense(10, activation='softmax'))

input = keras.Input(shape=(28, 28, 1))
x = layers.Conv2D(64, (3, 3), activation='relu', name='conv1')(input)
x = layers.Conv2D(64, (3, 3), activation='relu', name='conv2')(x)
x = layers.Conv2D(64, (3, 3),  activation='relu', name='conv3')(x)
x = layers.Flatten()(x)

x = layers.Dense(10, activation='softmax')(x)

model = keras.Model(inputs=input, outputs=x)


model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

H = model.fit(X_train, Y_train, validation_data=(X_val, Y_val),
          batch_size=32, epochs=10, verbose=1)


