import tensorflow as tf
import numpy as np
from keras.utils import np_utils
from tensorflow import keras
from tensorflow.keras import layers

print(tf.config.list_physical_devices('GPU'))

#POSITION ENCODING
def positional_encoding(pos, model_size):
    """ Compute positional encoding for a particular position
    Args:
        pos: position of a token in the sequence
        model_size: depth size of the model

    Returns:
        The positional encoding for the given token
    """
    PE = np.zeros((1, model_size))
    for i in range(model_size):
        if i % 2 == 0:
            PE[:, i] = np.sin(pos / 10000 ** (i / model_size))
        else:
            PE[:, i] = np.cos(pos / 10000 ** ((i - 1) / model_size))
    return PE


def generate_positional_encoding(max_length = 64, model_size= 28*28):
    max_length = 64
    MODEL_SIZE = 28 * 28

    pes = []
    for i in range(max_length):
        pes.append(positional_encoding(i, MODEL_SIZE))

    # (max_len_seq, embeding_dim)
    pes = np.concatenate(pes, axis=0)
    pes = tf.constant(pes, dtype=tf.float32)
    return pes

pes = generate_positional_encoding()

#ONLY USE DECODER TRANSFORMER

class MultiHeadAttention(tf.keras.Model):
    """ Class for Multi-Head Attention layer
    Attributes:
        key_size: d_key in the paper
        h: number of attention heads
        wq: the Linear layer for Q
        wk: the Linear layer for K
        wv: the Linear layer for V
        wo: the Linear layer for the output
    """

    def __init__(self, model_size, h):
        super(MultiHeadAttention, self).__init__()
        self.key_size = model_size // h
        self.h = h
        self.wq = tf.keras.layers.Dense(model_size)  # [tf.keras.layers.Dense(key_size) for _ in range(h)]
        self.wk = tf.keras.layers.Dense(model_size)  # [tf.keras.layers.Dense(key_size) for _ in range(h)]
        self.wv = tf.keras.layers.Dense(model_size)  # [tf.keras.layers.Dense(value_size) for _ in range(h)]
        self.wo = tf.keras.layers.Dense(model_size)

    def call(self, query, value, mask=None):
        """ The forward pass for Multi-Head Attention layer
        Args:
            query: the Q matrix
            value: the V matrix, acts as V and K
            mask: mask to filter out unwanted tokens
                  - zero mask: mask for padded tokens
                  - right-side mask: mask to prevent attention towards tokens on the right-hand side

        Returns:
            The concatenated context vector
            The alignment (attention) vectors of all heads
        """
        # query has shape (batch, query_len, model_size)
        # value has shape (batch, value_len, model_size)
        query = self.wq(query)
        key = self.wk(value)
        value = self.wv(value)
        #(5, 784, 28),
        # Split matrices for multi-heads attention
        batch_size = query.shape[0]

        # Originally, query has shape (batch, query_len, model_size)
        # We need to reshape to (batch, query_len, h, key_size)

        query = tf.reshape(query, [batch_size, -1, self.h, self.key_size])
        # In order to compute matmul, the dimensions must be transposed to (batch, h, query_len, key_size)
        query = tf.transpose(query, [0, 2, 1, 3])

        # Do the same for key and value
        key = tf.reshape(key, [batch_size, -1, self.h, self.key_size])
        key = tf.transpose(key, [0, 2, 1, 3])
        value = tf.reshape(value, [batch_size, -1, self.h, self.key_size])
        value = tf.transpose(value, [0, 2, 1, 3])

        # Compute the dot score
        # and divide the score by square root of key_size (as stated in paper)
        # (must convert key_size to float32 otherwise an error would occur)
        score = tf.matmul(query, key, transpose_b=True) / tf.math.sqrt(tf.dtypes.cast(self.key_size, dtype=tf.float32))
        # score will have shape of (batch, h, query_len, value_len)

        # Mask out the score if a mask is provided
        # There are two types of mask:
        # - Padding mask (batch, 1, 1, value_len): to prevent attention being drawn to padded token (i.e. 0)
        # - Look-left mask (batch, 1, query_len, value_len): to prevent decoder to draw attention to tokens to the right
        if mask is not None:
            score *= mask

            # We want the masked out values to be zeros when applying softmax
            # One way to accomplish that is assign them to a very large negative value
            score = tf.where(tf.equal(score, 0), tf.ones_like(score) * -1e9, score)

        # Alignment vector: (batch, h, query_len, value_len)
        alignment = tf.nn.softmax(score, axis=-1)

        # Context vector: (batch, h, query_len, key_size)
        context = tf.matmul(alignment, value)

        # Finally, do the opposite to have a tensor of shape (batch, query_len, model_size)
        context = tf.transpose(context, [0, 2, 1, 3])
        context = tf.reshape(context, [batch_size, -1, self.key_size * self.h])

        # Apply one last full connected layer (WO)
        heads = self.wo(context)
        #shape=(5, 784, 28)
        return heads, alignment
#truyen vao sequence, ouput sequence

class Encoder(tf.keras.Model):
    """ Class for the Encoder
    Args:
        model_size: d_model in the paper (depth size of the model)
        num_layers: number of layers (Multi-Head Attention + FNN)
        h: number of attention heads
        embedding: Embedding layer
        embedding_dropout: Dropout layer for Embedding
        attention: array of Multi-Head Attention layers
        attention_dropout: array of Dropout layers for Multi-Head Attention
        attention_norm: array of LayerNorm layers for Multi-Head Attention
        dense_1: array of first Dense layers for FFN
        dense_2: array of second Dense layers for FFN
        ffn_dropout: array of Dropout layers for FFN
        ffn_norm: array of LayerNorm layers for FFN
    """

    def __init__(self, model_size, num_layers, h):
        super(Encoder, self).__init__()
        self.model_size = model_size
        self.num_layers = num_layers
        self.h = h
        #neu dung anh thi khong can embedding
        # self.embedding = tf.keras.layers.Embedding(vocab_size, model_size)
        self.embedding_dropout = tf.keras.layers.Dropout(0.1)
        self.attention = [MultiHeadAttention(model_size, h) for _ in range(num_layers)]
        self.attention_dropout = [tf.keras.layers.Dropout(0.1) for _ in range(num_layers)]

        self.attention_norm = [tf.keras.layers.LayerNormalization(
            epsilon=1e-6) for _ in range(num_layers)]

        self.dense_1 = [tf.keras.layers.Dense(
            model_size * 4, activation='relu') for _ in range(num_layers)]
        self.dense_2 = [tf.keras.layers.Dense(
            model_size) for _ in range(num_layers)]
        self.ffn_dropout = [tf.keras.layers.Dropout(0.1) for _ in range(num_layers)]
        self.ffn_norm = [tf.keras.layers.LayerNormalization(
            epsilon=1e-6) for _ in range(num_layers)]

    def call(self, sequence, training=True, encoder_mask=None):
        """ Forward pass for the Encoder
        Args:
            sequence: source input sequences
            training: whether training or not (for Dropout)
            encoder_mask: padding mask for the Encoder's Multi-Head Attention

        Returns:
            The output of the Encoder (batch_size, length, model_size)
            The alignment (attention) vectors for all layers
        """
        #conv thi khong can embedding
        # embed_out = self.embedding(sequence)
        embed_out = tf.reshape(sequence, [sequence.shape[0], -1, sequence.shape[-1]])
        #shape=(5, 784, 64) bs, w*h, chanel
        embed_out *= tf.\
            math.sqrt(tf.cast(self.model_size, tf.float32))
        embed_out += tf.transpose(pes)
        embed_out = self.embedding_dropout(embed_out)

        sub_in = embed_out
        alignments = []

        for i in range(self.num_layers):
            #(5, 784, 64)
            sub_out, alignment = self.attention[i](sub_in, sub_in, encoder_mask)
            sub_out = self.attention_dropout[i](sub_out, training=training)
            sub_out = sub_in + sub_out
            sub_out = self.attention_norm[i](sub_out)

            alignments.append(alignment)
            ffn_in = sub_out

            ffn_out = self.dense_2[i](self.dense_1[i](ffn_in))
            ffn_out = self.ffn_dropout[i](ffn_out, training=training)
            ffn_out = ffn_in + ffn_out
            ffn_out = self.ffn_norm[i](ffn_out)

            sub_in = ffn_out

        return ffn_out, alignments


class Custom_model(tf.keras.Model):
    def __init__(self, width, height, channel, num_layers, num_heads, last_chanel_conv3):
        super(Custom_model, self).__init__()
        self.width = width
        self.height = height
        # self.model_size = model_size
        self.channel = channel
        # self.model_size = model_size
        self.num_layers = num_layers
        self.num_heads = num_heads
        self.conv1 = layers.Conv2D(32, (3,3), padding='same', activation='relu', input_shape=(width, height, channel))
        self.conv2 = layers.Conv2D(32, (3,3), padding='same', activation='relu')
        self.conv3 = layers.Conv2D(last_chanel_conv3, (3, 3), padding='same', activation='relu', name='conv3')
        #model_size = H * W
        self.last_chanel_conv3 = last_chanel_conv3
        self.encoder = Encoder(self.last_chanel_conv3, num_layers, num_heads)
        self.dense = layers.Dense(10, activation='relu')

    def call(self, input):
        x1 = self.conv1(input)
        x2 = self.conv2(x1)
        #(5, 28, 28, 64)
        x3 = self.conv3(x1 + x2)
        #shape=(5, 784, 64)
        encoder_output, _ = self.encoder(x3)
        #flatten
        encoder_output = tf.reshape(encoder_output, [encoder_output.shape[0], encoder_output.shape[1] * encoder_output.shape[2]])
        dense_output = self.dense(encoder_output)
        #output = np.argmax(dense_output)
        return dense_output



#load data
(X_train, y_train), (X_test, y_test) = keras.datasets.fashion_mnist.load_data()

# plt.figure()
# plt.imshow(X_train[0])
# plt.colorbar()
# plt.grid(False)
# plt.show()

X_train = X_train.reshape(X_train.shape[0], 28, 28, 1)
X_test = X_test.reshape(X_test.shape[0], 28, 28, 1)



Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

X_train = X_train / 255
X_test = X_test / 255

BATCH_SIZE = 16

dataset = tf.data.Dataset.from_tensor_slices((X_train, Y_train)).shuffle(1000).batch(BATCH_SIZE)

testset = tf.data.Dataset.from_tensor_slices((X_test[:-1000], Y_test[:-1000])).batch(BATCH_SIZE)

valset = tf.data.Dataset.from_tensor_slices((X_test[-1000:], Y_test[-1000:])).batch(BATCH_SIZE)


#parameter
H = 28
W = 28
C0 = 1
width = 28
height = 28
channel = 1
num_layers = 2
num_heads = 4
last_chanel_conv3 = 64

test_x = tf.random.uniform(shape=[5, 28, 28, 1])

custom_model = Custom_model(width, height, channel, num_layers, num_heads, last_chanel_conv3)


class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, d_model, warmup_steps=4000):
        super(CustomSchedule, self).__init__()

        self.d_model = d_model
        self.d_model = tf.cast(self.d_model, tf.float32)

        self.warmup_steps = warmup_steps

    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)

learning_rate = CustomSchedule(last_chanel_conv3)

optimizer = tf.keras.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98,
                                     epsilon=1e-9)

# loss_object = tf.keras.losses.categorical_crossentropy(
#     from_logits=False)


def loss_function(real, pred):
    loss_ = tf.keras.losses.categorical_crossentropy(real, pred)
    return tf.reduce_sum(loss_)


train_loss = tf.keras.metrics.Mean(name='train_loss')
train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(
    name='train_accuracy')

checkpoint_path = "./checkpoints/train"

ckpt = tf.train.Checkpoint(custom_model=custom_model)

ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=5)

# if a checkpoint exists, restore the latest checkpoint.
if ckpt_manager.latest_checkpoint:
    ckpt.restore(ckpt_manager.latest_checkpoint)
    print ('Latest checkpoint restored!!')


def train_step(images, labels):
    with tf.GradientTape() as tape:
        predictions = custom_model(images)
        loss = loss_function(labels, predictions)

    variables = custom_model.trainable_variables
    gradients = tape.gradient(loss, variables)
    optimizer.apply_gradients(zip(gradients, variables))
    #train_accuracy.update_state(labels, predictions)
    return loss, train_accuracy

epochs = 15

def test_step(images, lables):
    predict = custom_model(images)
    loss = loss_function(labels, predict)


for e in range(epochs):
    for batch, (images, labels) in enumerate(dataset.take(-1)):
        loss, train_accuracy = train_step(images, labels)
        if batch % 100 == 0:
            print("Epoch: {}, batch: {}, loss: {}, train_accuracy: {} ".format(e, batch, loss, ''))
            try:
                custom_model.save_weights('custom_model_weight')
            except:
                print('can save weight')
            # try:
            #     custom_model.save('path_to_my_model')
            # except:
            #     print('cant save model')


















